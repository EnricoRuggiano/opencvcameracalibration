/**
 *       @file  OpenCVCameraCalibration_exc.h
 *      @brief  The OpenCVCameraCalibration BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef OPENCVCAMERACALIBRATION_EXC_H_
#define OPENCVCAMERACALIBRATION_EXC_H_

#include <bbque/bbque_exc.h>
#include <string>
#include <condition_variable>
#include <mutex>
#include <vector>
#include "synchronized_queue.h"
#include "calibration_data.h"
#include "calibration_config.h"
#include "Processor.h"
#include "Sampler.h"

using bbque::rtlib::BbqueEXC;
using namespace std;
using namespace cv;

class OpenCVCameraCalibration : public BbqueEXC {

public:

	OpenCVCameraCalibration(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			std::string & setting_file);

private:
	string setting_file;
	mutex main_m;
	condition_variable cv_master;
	SynchronizedQueue<vector<Point2f>> imagePointsQueue;
	CalibrationData calibrationData;
	CalibrationConfig cfg;
	SynchronizedQueue<unsigned int> RunQueue;
	SynchronizedQueue<unsigned int> WaitQueue;
  SynchronizedQueue<unsigned int> ExitQueue;
	
	vector<Processor *> processors;
	vector<Sampler *> samplers;
	unsigned int numOfThreads;
	unsigned int physical_numOfThreads;
	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onSuspend();
	RTLIB_ExitCode_t onRelease();
};

#endif // OPENCVCAMERACALIBRATION_EXC_H_
