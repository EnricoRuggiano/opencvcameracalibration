#ifndef COMPUTE_ERROR_H
#define COMPUTE_ERROR_H
#include "calibration_data.h"
#include "synchronized_queue.h"
#include <vector>
#include <opencv2/core.hpp>

using namespace cv;
using namespace std;

double computeReprojectionErrors (
  const vector<vector<Point3f>> & objectPoints,
  const vector<vector<Point2f>> & imagePoints,
  const vector<Mat> & rvecs,
  const vector<Mat> & tvecs,
  const Mat & cameraMatrix,
  const Mat & distCoeffs,
  vector<float> &perViewErrors,
  bool fisheye
);
#endif