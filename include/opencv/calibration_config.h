#ifndef CALIBRATION_CONFIG_H
#define CALIBRATION_CONFIG_H
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>
#include <string>
#include <vector>

using namespace cv;
using namespace std;

class CalibrationConfig
{
  public:
    CalibrationConfig();    
    ~CalibrationConfig();

    enum Pattern { 
      CHESSBOARD
      , CIRCLES_GRID
      , ASYMMETRIC_CIRCLES_GRID
      , NOT_EXISTING 
    };
    
    enum InputType {
       CAMERA
       , VIDEO_FILE
       , IMAGE_LIST
       , INVALID 
    }; 

    enum SynchType {
      TIME
      , NUM_IMAGE
      , NONE
    };

    static CalibrationConfig Configure(
      string configFilePath);

public:
    Size boardSize;              // The size of the board -> Number of items by width and height
    Size imageSize;              // The resolution of input frames
    Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
    InputType inputType;
    SynchType synchType;
    vector <string> imageList;
    string str_synchType;
    string str_calibrationPattern;
    string outputFileName;       // The name of the file where to write
    string input;                // The input ->
    float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
    float aspectRatio;           // The aspect ratio
    float gridWidth;
    bool calibZeroTangentDist;   // Assume zero tangential distortion
    bool calibFixPrincipalPoint; // Fix the principal point at the center
    bool flipVertical;           // Flip the captured images around the horizontal axis
    bool showUndistorsed;        // Show undistorted images after calibration
    bool useFisheye;             // use fisheye camera model for calibration
    bool fixK1;                  // fix K1 distortion coefficient
    bool fixK2;                  // fix K2 distortion coefficient
    bool fixK3;                  // fix K3 distortion coefficient
    bool fixK4;                  // fix K4 distortion coefficient
    bool fixK5;                  // fix K5 distortion coefficient
    bool goodInput;
    // bool showGUI;
    bool enableSkip;
    int winSize;
    int flag;
    int numOfThreads_Processors;
    int numOfThreads_Samplers;
    int timeCycle;
    int imagesCycles;
    int numOfCycles;

  //private:
    void write( FileStorage & fs) const;                        //Write serialization for this class
    void read( const FileNode & node);                          //Read serialization for this class
    void validate();
    bool readStringList( const string & filename, vector<string>& l );
    bool isListOfImages( const string & filename);
  };


#endif