#ifndef SAMPLE_H
#define SAMPLE_H
#include "calibration_data.h"
#include "calibration_config.h"
#include "synchronized_queue.h"
#include <vector>
#include <opencv2/core.hpp>

using namespace cv;
using namespace std;

void sample (
   SynchronizedQueue<vector<Point2f>> *imagePoints
  , CalibrationData *calibrationData
  , CalibrationConfig calibrationConfig
);
#endif