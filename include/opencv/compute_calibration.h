#ifndef COMPUTE_CALIBRATION_H
#define COMPUTE_CALIBRATION_H
#include "calibration_data.h"
#include "calibration_config.h"
#include "synchronized_queue.h"
#include <vector>
#include <opencv2/core.hpp>
using namespace cv;
using namespace std;

void computeCalibration (
  SynchronizedQueue<vector<Point2f>> *imagePoints
  , CalibrationData *calibrationData
  , CalibrationConfig calibrationConfig
);   
#endif