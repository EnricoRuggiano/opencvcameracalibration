#ifndef CALIBRATION_DATA_H
#define CALIBRATION_DATA_H
#include <condition_variable>
#include <mutex>
#include <string>
#include <vector>
#include <opencv2/core.hpp>

using namespace cv;
using namespace std;

class CalibrationData 
{
  public:
    CalibrationData(string configFilePath);
    ~CalibrationData();

    void add(vector<Point2f> imagePoints);
    void add(Mat cameraMatrix);
    void add(float reprojError);
    
    vector<vector<Point2f>> get_ImagePoints();
    string                  get_ImagePath();
    unsigned int            get_ImagePaths_Size();          
    unsigned int            get_ReprojectionErrors_Size();
    Mat                     get_CameraMatrix();
    vector<float>           get_ReprojectionErrors();

  private:
    condition_variable mConditionVariable;
    mutex mMutex;  
  
    vector<vector<Point2f>> storedImagePoints;
    vector<string> imagePaths;
    
    // calibration data
    Mat cameraMatrix;
    //Mat distCoeffs;
    //vector<Mat> rvecs, tvecs;

    // errors
    vector<float> reprojErrs;
    //double totalAvgErr = 0;
};
#endif