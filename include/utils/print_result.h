#ifndef PRINT_RESULT_H
#define PRINT_RESULT_H
#include <string>
#include <vector>
using namespace std;

template <class T>
void print_result (string message, T a);

template <typename T>
void print_result_vector(string message, vector<T> val);

template <typename T>
void print_result_vector(string message, vector<vector<T>> val);

#endif