#ifndef HEURISTICS_H
#define HEURISTICS_H
unsigned int h_procs( unsigned int proc_nr );
unsigned int h_sampl( unsigned int proc_nr );
#endif