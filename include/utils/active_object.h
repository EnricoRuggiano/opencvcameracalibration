#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H

#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>
#include "synchronized_queue.h"

using namespace std;
class ActiveObject
{
  public: 
    ActiveObject (
      condition_variable & cv_master
      , SynchronizedQueue<unsigned int> & RunQueue
      , SynchronizedQueue<unsigned int> & WaitQueue
      , SynchronizedQueue<unsigned int> & ExitQueue 
    );
    ~ActiveObject();

    void restart();
    void pause();
    void stop();
    void skip();
    void resume();
    void exit();

  protected:
    thread t;
    atomic<bool> quit;
    atomic<bool> wait;
    atomic<bool> skip_;
    SynchronizedQueue<unsigned int>* ptr_nRunning; // synch taskqueue of int
    SynchronizedQueue<unsigned int>* ptr_nWaiting; // synch taskqueue of int
    SynchronizedQueue<unsigned int>* ptr_nExit;
    condition_variable* ptr_cv_master;
    condition_variable cv_skip;
    mutex m;
    mutex m_skip;
    mutex m_wait;

  private:
    virtual void run() = 0;
    ActiveObject(const ActiveObject &) = delete;
    ActiveObject & operator = (const ActiveObject &) = delete;
};
#endif