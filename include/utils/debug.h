#ifndef DEBUG_H
#define DEBUG_H
#include <string>
#include <vector>
using namespace std;

template <class T>
void debug (string message, T a);

template <typename T>
void debug_vector(string message, vector<T> val);

template <typename T>
void debug_vector(string message, vector<vector<T>> val);

#endif