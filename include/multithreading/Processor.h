#ifndef PROCESSOR_H
#define PROCESSOR_H
#include "active_object.h"
#include "calibration_data.h"
#include "calibration_config.h"
#include "synchronized_queue.h"

class Processor : public ActiveObject
{
  public: 
    Processor (
      string configFilePath
      , CalibrationData & calibrationData       
      , condition_variable & cv_master
      , SynchronizedQueue<unsigned int> & RunQueue
      , SynchronizedQueue<unsigned int> & WaitQueue
      , SynchronizedQueue<unsigned int> & ExitQueue
      , SynchronizedQueue<vector<Point2f>> & imagePointsQueue 
    );
    ~Processor();

  SynchronizedQueue<vector<Point2f>>* imagePointsQueue;
  CalibrationData* calibrationData;

  private:
    void run();
    CalibrationConfig calibrationConfig;
};
#endif