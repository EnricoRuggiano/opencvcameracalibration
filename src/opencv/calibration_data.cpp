#include "calibration_data.h"
#include "calibration_config.h"
#include "opencv2/calib3d.hpp"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>
#include "debug.h"
using namespace bbque::utils;

CalibrationData::CalibrationData(string configFilePath)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  CalibrationConfig cfg = CalibrationConfig::Configure(configFilePath);
  
  if (!cfg.goodInput)
  {
    logger->Error("Calibration Data - config data is invalid");
    return;
  }

  cameraMatrix = Mat::eye(3, 3, CV_64F);
  if (cfg.flag & CALIB_FIX_ASPECT_RATIO)
  {
    cameraMatrix.at<double>(0, 0) = cfg.aspectRatio;
    logger->Info("Calibration Data - Camera matrix fixed aspect Ratio %f", cfg.aspectRatio);
  }

  if (cfg.useFisheye)
  {
    Mat distCoeffs = Mat::zeros(4, 1, CV_64F);
    logger->Info("Calibration Data - use fisheye model");
  }
  else
  {
    Mat distCoeffs = Mat::zeros(8, 1, CV_64F);
  }

  imagePaths = cfg.imageList;

  debug("CameraMatrix", cameraMatrix);
  logger->Info("Calibration Data - setup correctly initialized ");
  return;
};

CalibrationData::~CalibrationData(){};

void CalibrationData::add(
  vector<Point2f> imagePoints
)
{
  unique_lock<mutex> l(mMutex);
  storedImagePoints.push_back(imagePoints);
}

void CalibrationData::add(
  Mat cameraMatrix
)
{
  unique_lock<mutex> l(mMutex);
  this->cameraMatrix = cameraMatrix;  
}

void CalibrationData::add(
  float reprojError
)
{
  unique_lock<mutex> l(mMutex);
  reprojErrs.push_back(reprojError);
  
}

vector<vector<Point2f>> 
CalibrationData::get_ImagePoints()
{
  unique_lock<mutex> l(mMutex);
  return storedImagePoints;
}

string 
CalibrationData::get_ImagePath()
{
  unique_lock<mutex> l(mMutex);
  if (imagePaths.empty())
  {
    return string("EMPTY");
  }
  string imagePath = imagePaths.back();
  imagePaths.pop_back();
  return imagePath;
}

unsigned int
CalibrationData::get_ImagePaths_Size()
{
  unique_lock<mutex> l(mMutex);
  return (unsigned int) imagePaths.size();
}

unsigned int
CalibrationData::get_ReprojectionErrors_Size()
{
  unique_lock<mutex> l(mMutex);
  return (unsigned int) reprojErrs.size();
}

Mat
CalibrationData::get_CameraMatrix()
{
  unique_lock<mutex> l(mMutex);
  return cameraMatrix;
}

vector<float>
CalibrationData::get_ReprojectionErrors()
{
  unique_lock<mutex> l(mMutex);
  return reprojErrs;
}