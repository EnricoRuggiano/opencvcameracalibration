#include "compute_calibration.h"
#include "compute_error.h"
#include "debug.h"
#include "opencv2/calib3d.hpp"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>

using namespace std;
using namespace cv;
using namespace bbque::utils;

void computeCalibration(
    SynchronizedQueue<vector<Point2f>> * imagePoints
    , CalibrationData * calibrationData
    , CalibrationConfig calibrationConfig )
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");

  // ********************************************* //
  // Estraggo i dati che servono per la Calibrazione
  // dalla Calibration Data shared tra i threads

  Mat calibrationMatrix = calibrationData->get_CameraMatrix();
  vector<float> reprjErrors = calibrationData->get_ReprojectionErrors();
  vector<vector<Point2f>> storedImagePoints = calibrationData->get_ImagePoints();
  // ********************************************* //
  float rms;
  vector<Point3f> newObjPoints;
  Mat distCoeffs;
  vector<Mat> rvecs, tvecs;

  // ******************************************** //
  if (calibrationConfig.useFisheye)
  {
    distCoeffs = Mat::zeros(4, 1, CV_64F);
  }
  else
  {
    distCoeffs = Mat::zeros(8, 1, CV_64F);
  }
  
  // ******************************************** //

  if (imagePoints->size() > 0)
  {
    // logger->Info("INIZIO CALIBRATION");
    // debug("START CALIBRATION MATRIX", calibrationMatrix);
    // debug_vector("START REPROJERRORS", reprjErrors);
    //debug_vector("START IMAGEPOINTS", storedImagePoints);

  // ******************************************** //
    //logger->Info("CALIBRATION RICEVE NUOVI IMAGE POINTS");
    
    // salva nella stored image points
    vector<Point2f> newImagePoints = imagePoints->get();
    storedImagePoints.push_back(newImagePoints);
    //debug_vector("NUOVI IMAGE POINTS RICEVUTI:", newImagePoints);

    // object points init
    vector<vector<Point3f>> objectPoints(1);
    //objectPoints[0].resize(0);
    objectPoints[0].clear();

    switch (calibrationConfig.calibrationPattern)
    {
    case CalibrationConfig::CHESSBOARD:
    case CalibrationConfig::CIRCLES_GRID:
      for (int i = 0; i < calibrationConfig.boardSize.height; ++i)
        for (int j = 0; j < calibrationConfig.boardSize.width; ++j)
          objectPoints[0].push_back(cv::Point3f(float(j * calibrationConfig.squareSize),
                                                float(i * calibrationConfig.squareSize), 0));
      break;

    case CalibrationConfig::ASYMMETRIC_CIRCLES_GRID:
      for (int i = 0; i < calibrationConfig.boardSize.height; i++)
        for (int j = 0; j < calibrationConfig.boardSize.width; j++)
          objectPoints[0].push_back(cv::Point3f(float((2 * j + i % 2) * calibrationConfig.squareSize),
                                                float(i * calibrationConfig.squareSize), 0));
      break;
    default:
      break;
    }

    objectPoints[0][calibrationConfig.boardSize.width - 1].x =
       objectPoints[0][0].x + calibrationConfig.gridWidth;
    newObjPoints = objectPoints[0];
    objectPoints.resize(storedImagePoints.size(), objectPoints[0]);

    //debug_vector("OBJECT POINTS", objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
    if (calibrationConfig.useFisheye)
    {
      Mat _rvecs, _tvecs;
      rms = fisheye::calibrate(
          objectPoints,
          storedImagePoints,
          calibrationConfig.imageSize,
          calibrationMatrix,
          distCoeffs,
          _rvecs,
          _tvecs,
          calibrationConfig.flag);

      rvecs.reserve(_rvecs.rows);
      tvecs.reserve(_tvecs.rows);
      for (int i = 0; i < int(objectPoints.size()); i++)
      {
        rvecs.push_back(_rvecs.row(i));
        tvecs.push_back(_tvecs.row(i));
      }
    }
    else
    {
      int iFixedPoint = -1;
      // if (CalibrationConfig.release_object)
      // {
      //   iFixedPoint = CalibrationConfig.boardSize.width - 1;
      // }

      rms = calibrateCameraRO(
          objectPoints,
          storedImagePoints,
          calibrationConfig.imageSize,
          iFixedPoint,
          calibrationMatrix,
          distCoeffs,
          rvecs,
          tvecs,
          newObjPoints,
          calibrationConfig.flag | cv::CALIB_USE_LU);
    }

    // reprojection error
    reprjErrors.push_back(rms);

    bool ok = cv::checkRange(calibrationMatrix) && cv::checkRange(distCoeffs);

    objectPoints.clear();
    objectPoints.resize(storedImagePoints.size(), newObjPoints);
    float totalAvgErr = computeReprojectionErrors(
        objectPoints,
        storedImagePoints,
        rvecs,
        tvecs,
        calibrationMatrix,
        distCoeffs,
        reprjErrors,
        calibrationConfig.useFisheye);
    
    // ******************************************** //
    // STORE DEI SHARED DATA TRA I THREADS          //
    // ******************************************** //
    calibrationData->add(calibrationMatrix);
    calibrationData->add(newImagePoints);
    calibrationData->add(rms);

    logger->Info("Calibrazione Completata");
    //logger->Info("RISULTATI:");
    debug("CalibratationMatrix", calibrationMatrix);
    debug("RMS Errror", rms);
    //debug("Average Error", totalAvgErr);
  }
}
