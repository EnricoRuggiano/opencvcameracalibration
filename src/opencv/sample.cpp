#include "sample.h"
#include "debug.h"
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>

using namespace std;
using namespace cv;
using namespace bbque::utils;

void sample(
   SynchronizedQueue<vector<Point2f>> *imagePoints
  , CalibrationData *calibrationData
  , CalibrationConfig calibrationConfig)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");

  // ********************************************* //
  // Estraggo i dati che servono per la Calibrazione
  // dalla Calibration Data shared tra i threads

  //logger->Info("INIZIO SAMPLING");
  // logger->Info("SAMPLE - num of images path %d", (int) calibrationData->get_ImagePoints_Size());
  //debug_vector("Image Paths", imagePaths);
  
  
  // ******************************************** //
  //  LEGGO INPUT                                 //
  // ******************************************** //

  Mat view;
  vector<Point2f> pointBuf;
  bool found;
  int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;
  string imagePath("EMPTY");

  // ********************************************** //
  
  if (calibrationConfig.inputType == CalibrationConfig::IMAGE_LIST)
  {
    //logger->Info("SAMPLE - num of images path %d", (int) calibrationData->get_ImagePaths_Size());
    imagePath = calibrationData->get_ImagePath();
    if(imagePath == "EMPTY")
    {
      //logger->Warn("Sampling - images to sample are finished");
      return;
    }
    view = imread(imagePath, cv::IMREAD_COLOR);
    //logger->Info("Sampling - ImageList inputType");
    //logger->Info("Sampling - opening image %s", imagePath.c_str());
  }
  else
  {
    logger->Error("Sampling - Bad inputType");
    return;
  }

  // ********************************************** //
  //  OPERAZIONI SULLA VIEW                         //
  // ***********************************************//

  if (view.empty())
  {
    logger->Error("Sampling - view is empty");
    return;
  }

  if (view.size().width != calibrationConfig.imageSize.width ||
      view.size().height != calibrationConfig.imageSize.height)
  {
    logger->Error("Sampling - view has size different to config file");
    logger->Error("VIEW: %d x %d CONFIG_IMAGE_SIZE: %d x %d",
                  view.size().width, view.size().height, calibrationConfig.imageSize.width, calibrationConfig.imageSize.height);
    return;
  }

  // flip if it is set
  if (calibrationConfig.flipVertical)
  {
    flip(view, view, 0);
  }

  // set flag if fisheye is set
  if (!calibrationConfig.useFisheye)
  {
    // fast check erroneously fails with high distortions like fisheye
    chessBoardFlags |= CALIB_CB_FAST_CHECK;
  }

  // extract from pattern
  switch (calibrationConfig.calibrationPattern) // Find feature points on the input format
  {
  case CalibrationConfig::CHESSBOARD:
  {
    found = findChessboardCorners(view, calibrationConfig.boardSize, pointBuf, chessBoardFlags);
    break;
  }
  case CalibrationConfig::CIRCLES_GRID:
  {
    found = findCirclesGrid(view, calibrationConfig.boardSize, pointBuf);
    break;
  }
  case CalibrationConfig::ASYMMETRIC_CIRCLES_GRID:
  {
    found = findCirclesGrid(view, calibrationConfig.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID);
    break;
  }
  default:
  {
    found = false;
    break;
  }
  }

  // if found data
  if (found)
  {
    //logger->Info("Sample - FOUND DATA");
    //debug_vector("Punti immagine calcolati :", pointBuf);

    // here we have some optimization ...
    if (calibrationConfig.calibrationPattern == CalibrationConfig::CHESSBOARD)
    {
      Mat viewGray;
      cvtColor(view, viewGray, COLOR_BGR2GRAY);
      cornerSubPix(
          viewGray,
          pointBuf,
          Size(calibrationConfig.winSize, calibrationConfig.winSize),
          Size(-1, -1),
          TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT,
                           30,
                           0.0001));

          //debug_vector("Punti immagine ottimizzati :", pointBuf);
    }


  // ********************************************** //
  //  Esporto shared data                           //
  // ********************************************** //
    imagePoints->push(pointBuf);

    // Fancy GUI
    drawChessboardCorners(view, calibrationConfig.boardSize, Mat(pointBuf), found);
  }

  // Fancy OpenCV debug
  // if (calibrationConfig.showGUI)
  // {
  //   string msg = "Sampling image: \n" + imagePath;
  //   int baseLine = 0;
  //   const Scalar RED(0, 0, 255);
  //   const Scalar GREEN(0, 255, 0);
  //   Size textSize = getTextSize(imagePath, 1, 1, 1, &baseLine);
  //   Point textOrigin(view.cols - 2 * textSize.width - 10, view.rows - 2 * baseLine - 10);
  //   cv::putText(view, imagePath, textOrigin, 1, 1, found ? GREEN : RED);
    
  //   imshow("OpenCVCameraCalibration", view);

  // }
}
