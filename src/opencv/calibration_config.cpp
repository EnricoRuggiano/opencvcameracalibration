#include "calibration_config.h"
#include <opencv2/calib3d.hpp>
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>
using namespace bbque::utils;

static inline void read(
  const FileNode& node
, CalibrationConfig& x
, const CalibrationConfig& default_value = CalibrationConfig())
{
    if(node.empty())
        x = default_value;
    else
        x.read(node);
}

CalibrationConfig::CalibrationConfig():goodInput(false){};

CalibrationConfig CalibrationConfig::Configure(
  string configFilePath
)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  CalibrationConfig cfg;
  if (configFilePath.empty())
  {
    return cfg;
  }
  // open the file
  FileStorage configFile = FileStorage(configFilePath, FileStorage::READ);

  if (!configFile.isOpened())
  {
    // default params
    logger->Error("Setup - config file %s not found", configFilePath.c_str());
  }
  else
  {
    logger->Info("Setup - config file %s found", configFilePath.c_str());
    configFile["Settings"] >> cfg;
    configFile.release(); // close Settings file
  }
  return cfg;
}

CalibrationConfig::~CalibrationConfig() {};

//Write serialization for this class
void CalibrationConfig::write(FileStorage & fs) const 
{
  fs << "{"
     << "BoardSize_Width" << boardSize.width
     << "BoardSize_Height" << boardSize.height
     << "ImageSize_Width" << imageSize.width
     << "ImageSize_Height" << imageSize.height
     << "Square_Size" << squareSize
     << "win_size" << winSize
     << "Calibrate_Pattern" << str_calibrationPattern
     << "Fix_K1" << fixK1
     << "Fix_K2" << fixK2
     << "Fix_K3" << fixK3
     << "Fix_K4" << fixK4
     << "Fix_K5" << fixK5
     << "Show_UndistortedImage" << showUndistorsed
     << "Input_FlipAroundHorizontalAxis" << flipVertical
     << "Calibrate_UseFisheyeModel" << useFisheye
     << "Input" << input
     << "Calibrate_AssumeZeroTangentialDistortion" << calibZeroTangentDist
     << "Calibrate_FixPrincipalPointAtTheCenter" << calibFixPrincipalPoint
     << "enableSkip" << enableSkip
     << "imagesCycles" << imagesCycles
     << "timeCycle" << timeCycle
     << "numOfCycles" << numOfCycles
     << "numOfThreads_Processors" << numOfThreads_Processors
     << "numOfThreads_Samplers" << numOfThreads_Samplers
    //  << "showGUI" << showGUI
     << "synchType" << str_synchType
     << "}";
}

//Read serialization for this class
void CalibrationConfig::read ( const FileNode & node ) 
{
  node["BoardSize_Width"] >> boardSize.width;
  node["BoardSize_Height"] >> boardSize.height;
  node["ImageSize_Width"] >> imageSize.width;
  node["ImageSize_Height"] >> imageSize.height;
  node["Calibrate_Pattern"] >> str_calibrationPattern;
  node["Square_Size"] >> squareSize;
  node["Calibrate_FixAspectRatio"] >> aspectRatio;
  node["Write_outputFileName"] >> outputFileName;
  node["Calibrate_AssumeZeroTangentialDistortion"] >> calibZeroTangentDist;
  node["Calibrate_FixPrincipalPointAtTheCenter"] >> calibFixPrincipalPoint;
  node["Calibrate_UseFisheyeModel"] >> useFisheye;
  node["Input_FlipAroundHorizontalAxis"] >> flipVertical;
  node["Show_UndistortedImage"] >> showUndistorsed;
  node["Input"] >> input;
  node["Fix_K1"] >> fixK1;
  node["Fix_K2"] >> fixK2;
  node["Fix_K3"] >> fixK3;
  node["Fix_K4"] >> fixK4;
  node["Fix_K5"] >> fixK5;
  node["win_size"] >> winSize;
  node["enableSkip"] >> enableSkip;
  node["imagesCycles"] >> imagesCycles;
  node["timeCycle"] >> timeCycle;
  node["numOfCycles"] >> numOfCycles;
  node["numOfThreads_Processors"] >> numOfThreads_Processors;
  node["numOfThreads_Samplers"] >> numOfThreads_Samplers;
  // node["showGUI"] >> showGUI;
  node["synchType"] >> str_synchType;
  validate();
}

void CalibrationConfig::validate()
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");

  goodInput = true;
  if (boardSize.width <= 0 || boardSize.height <= 0)
  {
    logger->Error("Calibration Config - Invalid board size with width: %f height: %f", boardSize.width, boardSize.height);
    goodInput = false;
  }
  if (squareSize <= 10e-6)
  {
    logger->Error("Calibration Config - Invalid square size: %f", squareSize);
    goodInput = false;
  }

  if (input.empty())
  {
    // Check for valid input
    inputType = INVALID;
    logger->Error("Calibration Config - Empty input value");
  }
  else
  {
    if (isListOfImages(input) && readStringList(input, imageList))
    {
      inputType = IMAGE_LIST;      
      logger->Info("Calibration Config - input type IMAGE_LIST of %d", (int) imageList.size());
    }
    else
    {
      logger->Error("Calibration Config - Impossible to open VIDEO_FILE %s", input.c_str());
      inputType = INVALID;
    }
  }

  if (inputType == INVALID)
  {
    logger->Error("Calibration Config - input type is INVALID");
    goodInput = false;
  }

  flag = 0;
  if (calibFixPrincipalPoint)
  {
    logger->Info("Calibration Config - setting fix principal point flag");
    flag |= CALIB_FIX_PRINCIPAL_POINT;
  }

  if (calibZeroTangentDist)
  {
    logger->Info("Calibration Config - setting zero tangent distance flag");
    flag |= CALIB_ZERO_TANGENT_DIST;
  }
  if (aspectRatio)
  {
    logger->Info("Calibration Config - setting fix aspect ratio flag");
    flag |= CALIB_FIX_ASPECT_RATIO;
  }
  if (fixK1)
  {
    logger->Info("Calibration Config - setting fix k1 flag");
    flag |= CALIB_FIX_K1;
  }
  if (fixK2)
  {
    logger->Info("Calibration Config - setting fix k2 flag");
    flag |= CALIB_FIX_K2;
  }
  if (fixK3)
  {
    logger->Info("Calibration Config - setting fix k3 flag");
    flag |= CALIB_FIX_K3;
  }
  if (fixK4)
  {
    logger->Info("Calibration Config - setting fix k4 flag");
    flag |= CALIB_FIX_K4;
  }
  if (fixK5)
  {
    logger->Info("Calibration Config - setting fix k5 flag");
    flag |= CALIB_FIX_K5;
  }

  if (useFisheye)
  {
    // the fisheye model has its own enum, so overwrite the flags
    logger->Info("Calibration Config - fisheye model ");
    flag = fisheye::CALIB_FIX_SKEW | fisheye::CALIB_RECOMPUTE_EXTRINSIC;
    if (fixK1)
    {
      flag |= fisheye::CALIB_FIX_K1;
      logger->Info("Calibration Config - setting fisheye model fix k1 flag");
    }

    if (fixK2)
    {
      flag |= fisheye::CALIB_FIX_K2;
      logger->Info("Calibration Config - setting fisheye model fix k2 flag");
    }
    if (fixK3)
    {
      flag |= fisheye::CALIB_FIX_K3;
      logger->Info("Calibration Config - setting fisheye model fix k3 flag");
    }
    if (fixK4)
    {
      flag |= fisheye::CALIB_FIX_K4;
      logger->Info("Calibration Config - setting fisheye model fix k4 flag");
    }
    if (calibFixPrincipalPoint)
    {
      flag |= fisheye::CALIB_FIX_PRINCIPAL_POINT;
      logger->Info("Calibration Config - setting fisheye model fix principal point flag");
    }
  }

  calibrationPattern = NOT_EXISTING;
  if (!str_calibrationPattern.compare("CHESSBOARD"))
  {
    calibrationPattern = Pattern::CHESSBOARD;

    logger->Info("Calibration Config - pattern CHESSBOARD");
  }
  if (!str_calibrationPattern.compare("CIRCLES_GRID"))
  {
    calibrationPattern = Pattern::CIRCLES_GRID;
    logger->Info("Calibration Config - pattern CIRCLES GRID");
  }

  if (!str_calibrationPattern.compare("ASYMMETRIC_CIRCLES_GRID"))
  {
    calibrationPattern = Pattern::ASYMMETRIC_CIRCLES_GRID;
    logger->Info("Calibration Config - pattern ASYMMETRIC CIRCLES GRID");
  }
  if (calibrationPattern == Pattern::NOT_EXISTING)
  {
    logger->Error("Calibration Config - pattern NOT EXISTING");
    goodInput = false;
  }

  synchType = NONE;
  if (!str_synchType.compare("TIME"))
  {
    synchType = SynchType::TIME;
    logger->Info("Calibration Config - synchType TIME");
  }
  if (!str_synchType.compare("NUM_IMAGE"))
  {
    synchType = SynchType::NUM_IMAGE;
    logger->Info("Calibration Config - synchType NUM_IMAGE");
  }
  if(synchType == SynchType::NONE)
  {
    logger->Error("Calibration Config - synchType INVALID");
    goodInput = false;
  }

  if (numOfThreads_Processors <= 0)
  {
    logger->Error("Calibration Config - Invalid number of threads Processors: %d", numOfThreads_Processors);
    goodInput = false; 
  }

  if (numOfThreads_Samplers <= 0)
  {
    logger->Error("Calibration Config - Invalid number of threads Processors: %d", numOfThreads_Samplers);
    goodInput = false; 
  }
  
  if (timeCycle <= 0)
  {
    logger->Error("Calibration Config - Invalid number of timeCycle: %d", timeCycle);
    goodInput = false; 
  }
  
  if (imagesCycles <= 0)
  {
    logger->Error("Calibration Config - Invalid number of imagesCycles: %d", imagesCycles);
    goodInput = false; 
  }

  if (numOfCycles <= 0)
  {
    logger->Error("Calibration Config - Invalid number of numOfCycles: %d", numOfCycles);
    goodInput = false; 
  }
  
  // others
  gridWidth = squareSize * (boardSize.width - 1);
  //release_object = false;
  winSize = 11;
}

bool CalibrationConfig::readStringList(
  const string & filename 
  , vector<string> & l
)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");

  l.clear();
  FileStorage fs(filename, FileStorage::READ);
  if (!fs.isOpened())
  {
    logger->Error("CalibrationConfig - %s not found", filename.c_str());
    return false;
  }
  FileNode n = fs.getFirstTopLevelNode();
  FileNodeIterator it = n.begin();
  FileNodeIterator it_end = n.end();
  for (; it != it_end; ++it)
  {
    l.push_back((string) * it);
  }
  return true;
}

bool CalibrationConfig::isListOfImages(const string &filename )
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  string s(filename);
  // Look for file extension
  if (
      s.find(".xml")  == string::npos &&
      s.find(".yaml") == string::npos &&
      s.find(".yml")  == string::npos)
  {
    logger->Error("CalibrationConfig - Image List file %s is not a parsable config file", s.c_str());
    return false;
  }
  else
  {
    return true;
  }
}