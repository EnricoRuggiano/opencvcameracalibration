#include "Sampler.h"
#include "sample.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>
using namespace bbque::utils;

Sampler::Sampler (
   string configFilePath
  , CalibrationData & calibrationData       
  , condition_variable & cv_master
  , SynchronizedQueue<unsigned int> & RunQueue
  , SynchronizedQueue<unsigned int> & WaitQueue
  , SynchronizedQueue<unsigned int> & ExitQueue
  , SynchronizedQueue<vector<Point2f>> & imagePointsQueue 
) 
: ActiveObject ( 
    cv_master
    , RunQueue
    , WaitQueue
    , ExitQueue)
  , calibrationData(& calibrationData)
  , calibrationConfig(CalibrationConfig::Configure(configFilePath))
  , imagePointsQueue(& imagePointsQueue)
{
};

Sampler::~Sampler () {};

void Sampler::run()
{  
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  unique_lock<mutex> l (m);
  ptr_nRunning->push(0);
  ptr_cv_master->wait(l);
  logger->Info("Sampler e' partito");
  while (!quit.load())
  { 
    if (skip_.load())
    {
      // skip 
      unique_lock<mutex> l1 (m_skip);
      logger->Info("Sampler e' stato skippato");
      cv_skip.wait(l1);
      l1.unlock();
      logger->Info("Sampler e' stato ripreso");
      continue;
    }


    if (wait.load()) 
    {
      // wait
      unique_lock<mutex> l2 (m_wait);
      ptr_nWaiting->push(0);
      logger->Info("Sampler e' stato stoppato");
      ptr_cv_master->wait(l2);

      // esco dal waiting
      ptr_nWaiting->get();
      wait.store(false);
      l2.unlock();
      logger->Info("Sampler e' ripartito");
      continue;
    }

    // ************** //
    // do stuff here  //
    // ************** //

    sample(imagePointsQueue, calibrationData, calibrationConfig);
  }
  ptr_nExit->push(0);
  logger->Info("Sampler e' tornato");
};
