#include "heuristics.h"
#include <math.h>

unsigned int h_procs( unsigned int proc_nr )
{
  if(proc_nr == 1)
  {
    return 1;
  }  
  return (unsigned int) floor(proc_nr / 2);
}

unsigned int h_sampl( unsigned int proc_nr )
{
  return proc_nr;
}