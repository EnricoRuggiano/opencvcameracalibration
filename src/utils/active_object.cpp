#include "active_object.h"

ActiveObject::ActiveObject (
  condition_variable & cv_master
  , SynchronizedQueue<unsigned int> & RunQueue
  , SynchronizedQueue<unsigned int> & WaitQueue
  , SynchronizedQueue<unsigned int> & ExitQueue ) :
  t(&ActiveObject::run, this)
  , ptr_cv_master(& cv_master)
  , wait(false)
  , quit(false)
  , skip_(false)
  , ptr_nRunning(& RunQueue)
  , ptr_nWaiting(& WaitQueue)
  , ptr_nExit(& ExitQueue)
{
};

ActiveObject::~ActiveObject () {};

void ActiveObject::restart()
{
  if(!wait.load())
  {
    return;
  } 
  wait.store(false);
}

void ActiveObject::pause()
{
  if(wait.load())
  {
    return;
  } 
  wait.store(true);
}

void ActiveObject::stop()
{
  if(quit.load())
  {
    return;
  } 
  quit.store(true);
}

void ActiveObject::exit()
{
  if(!quit.load())
  {
    return;
  }
  quit.store(true);
  
  // sto continue here the run is terminated 
  unique_lock<mutex> l (m);
  t.join();
}

void ActiveObject::skip()
{
  if(skip_.load())
  {
    return;
  } 
  skip_.store(true);
}

void ActiveObject::resume()
{
  if(!skip_.load())
  {
    return;
  }
  skip_.store(false);
  cv_skip.notify_one();
}