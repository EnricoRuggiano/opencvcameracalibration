#include "print_result.h"
#include <sstream>
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>
#include <opencv2/core.hpp>

using namespace std;
using namespace cv;
using namespace bbque::utils;

template <typename T>
void print_result(string message, T val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  stringstream ss;
  ss << val;

  string str(ss.str());
  logger->Notice("%s\n%s", message.c_str(), str.c_str());
};

template <typename T>
void print_result_vector(string message, vector<T> val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  logger->Notice("%s\n", message.c_str());

  stringstream ss;
  unsigned int i = 0;
  for (auto &elem : val)
  {
    ss << elem;
    string s(ss.str()); 
    ss.str("");
    logger->Notice("%d]\t%s", i, s.c_str());
    i++;
  }
};

template <typename T>
void print_result_vector(string message, vector<vector<T>> val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  logger->Notice("%s\n", message.c_str());
  unsigned int i = 0;
  for (auto &elem : val)
  {
    string s = "Vector_" + i;
    print_result_vector(s, elem);
    i++;
  }
};

template void print_result<float>(string, float);
template void print_result<Mat>(string, Mat);
template void print_result<Point2f>(string, Point2f);
template void print_result<Point3f>(string, Point3f);

template void print_result_vector<float>(string, vector<float>);
template void print_result_vector<Mat>(string, vector<Mat>);
template void print_result_vector<Point2f>(string, vector<Point2f>);
template void print_result_vector<Point3f>(string, vector<Point3f>);
template void print_result_vector<string>(string, vector<string>);

template void print_result_vector<float>(string, vector<vector<float>>);
template void print_result_vector<Mat>(string, vector<vector<Mat>>);
template void print_result_vector<Point2f>(string, vector<vector<Point2f>>);
template void print_result_vector<Point3f>(string, vector<vector<Point3f>>);


