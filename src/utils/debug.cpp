#include "debug.h"
#include <sstream>
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>
#include <memory>
#include <opencv2/core.hpp>

using namespace std;
using namespace cv;
using namespace bbque::utils;

template <typename T>
void debug(string message, T val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  stringstream ss;
  ss << val;

  string str(ss.str());
  logger->Info("Debug - %s\n%s", message.c_str(), str.c_str());
};

template <typename T>
void debug_vector(string message, vector<T> val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  logger->Info("Debug - %s\n", message.c_str());

  stringstream ss;
  unsigned int i = 0;
  for (auto &elem : val)
  {
    ss << elem;
    string s(ss.str()); 
    ss.str("");
    logger->Info("%d]\t%s", i, s.c_str());
    i++;
  }
};

template <typename T>
void debug_vector(string message, vector<vector<T>> val)
{
  unique_ptr<Logger> logger = Logger::GetLogger("opencvcameracalibration");
  logger->Info("Debug - %s\n", message.c_str());
  unsigned int i = 0;
  for (auto &elem : val)
  {
    string s = "Vector_" + i;
    debug_vector(s, elem);
    i++;
  }
};

template void debug<float>(string, float);
template void debug<Mat>(string, Mat);
template void debug<Point2f>(string, Point2f);
template void debug<Point3f>(string, Point3f);

template void debug_vector<float>(string, vector<float>);
template void debug_vector<Mat>(string, vector<Mat>);
template void debug_vector<Point2f>(string, vector<Point2f>);
template void debug_vector<Point3f>(string, vector<Point3f>);
template void debug_vector<string>(string, vector<string>);

template void debug_vector<float>(string, vector<vector<float>>);
template void debug_vector<Mat>(string, vector<vector<Mat>>);
template void debug_vector<Point2f>(string, vector<vector<Point2f>>);
template void debug_vector<Point3f>(string, vector<vector<Point3f>>);


