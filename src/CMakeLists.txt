
#----- Check for the required RTLib library
find_package(BbqRTLib REQUIRED)
find_package(OpenCV REQUIRED)

#---- add compiler definition
#add_compile_definitions(BBQUE)

#----- Add compilation dependencies
set(OPENCVCAMERACALIBRATION_INCLUDE_DIR 
			../include/  
			../include/utils
			../include/opencv
			../include/multithreading
	)

include_directories(
	${BBQUE_RTLIB_INCLUDE_DIR} 
	${OPENCVCAMERACALIBRATION_INCLUDE_DIR}
)

#----- Add "opencvcameracalibration" target application
set(OPENCVCAMERACALIBRATION_SRC 
			version 
			OpenCVCameraCalibration_exc 
			OpenCVCameraCalibration_main 
			utils/active_object 
			utils/debug
			utils/heuristics
			utils/print_result
			utils/synchronized_queue
			opencv/calibration_data
			opencv/calibration_config
			opencv/compute_calibration
			opencv/compute_error
			opencv/sample
			multithreading/Processor 
			multithreading/Sampler
	)

add_executable(opencvcameracalibration ${OPENCVCAMERACALIBRATION_SRC})

target_compile_definitions(opencvcameracalibration PUBLIC BBQUE)

#----- Linking dependencies
target_link_libraries(
	opencvcameracalibration
	${Boost_LIBRARIES}
	${BBQUE_RTLIB_LIBRARY}
  ${OpenCV_LIBS}
)

# Pthread
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")


# Use link path ad RPATH
set_property(TARGET opencvcameracalibration PROPERTY
	INSTALL_RPATH_USE_LINK_PATH TRUE)

#----- Install the OpenCVCameraCalibration files
install (TARGETS opencvcameracalibration RUNTIME
	DESTINATION ${OPENCVCAMERACALIBRATION_PATH_BINS})

#----- Generate and Install OpenCVCameraCalibration configuration file
configure_file (
	"${PROJECT_SOURCE_DIR}/OpenCVCameraCalibration.conf.in"
	"${PROJECT_BINARY_DIR}/OpenCVCameraCalibration.conf"
)
install (FILES "${PROJECT_BINARY_DIR}/OpenCVCameraCalibration.conf"
	DESTINATION ${OPENCVCAMERACALIBRATION_PATH_CONFIG})
