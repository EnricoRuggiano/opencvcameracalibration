/**
 *       @file  OpenCVCameraCalibration_exc.cc
 *      @brief  The OpenCVCameraCalibration BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include "OpenCVCameraCalibration_exc.h"
#include <cstdio>
#include <bbque/utils/utility.h>
#include <chrono>
#include <thread>
#include "heuristics.h"
#include "print_result.h"

OpenCVCameraCalibration::OpenCVCameraCalibration(
	std::string const & name
	,	std::string const & recipe
	,	RTLIB_Services_t *rtlib
	,	std::string & setting_file) :
	BbqueEXC(name, recipe, rtlib)
	, setting_file(setting_file)
	, calibrationData(setting_file)
{
	logger->Warn("New OpenCVCameraCalibration::OpenCVCameraCalibration()");

	// NOTE: since RTLib 1.1 the derived class construct should be used
	// mainly to specify instantiation parameters. All resource
	// acquisition, especially threads creation, should be palced into the
	// new onSetup() method.
	logger->Notice("EXC Unique IDentifier (UID): %u", GetUniqueID());
}

RTLIB_ExitCode_t OpenCVCameraCalibration::onSetup() {
	// This is just an empty method in the current implementation of this
	// testing application. However, this is intended to collect all the
	// application specific initialization code, especially the code which
	// acquire system resources (e.g. thread creation)
	logger->Warn("OpenCVCameraCalibration::onSetup()");
	logger->Notice("Setting file: %s", setting_file.c_str());
	cfg = CalibrationConfig::Configure(setting_file);
	if (!cfg.goodInput)
	{
		logger->Error("Configure File %s is not valid", setting_file.c_str());
		return RTLIB_ERROR;
	}

	// ********************************** //
	// crea i thread											//
	// ********************************** //
	numOfThreads = (unsigned int) (cfg.numOfThreads_Processors + cfg.numOfThreads_Samplers);
	physical_numOfThreads = numOfThreads;

	for (unsigned int i = 0; i < (unsigned int) cfg.numOfThreads_Processors; i++)
	{
		processors.push_back(new Processor(
			setting_file 
			, std::ref(calibrationData)
			, std::ref(cv_master)
			, std::ref(RunQueue)
			, std::ref(WaitQueue)
			, std::ref(ExitQueue)
			, std::ref(imagePointsQueue)
		));
	}
	for (unsigned int i = 0; i < (unsigned int) cfg.numOfThreads_Samplers; i++)
	{
		samplers.push_back(new Sampler(
			setting_file 
			, std::ref(calibrationData)
			, std::ref(cv_master)
			, std::ref(RunQueue)
			, std::ref(WaitQueue)
			, std::ref(ExitQueue)
			, std::ref(imagePointsQueue)
		));
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t OpenCVCameraCalibration::onConfigure(int8_t awm_id) {
	logger->Warn("OpenCVCameraCalibration::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);

	int32_t proc_quota, proc_nr, mem;
	GetAssignedResources(PROC_ELEMENT, proc_quota);
	GetAssignedResources(PROC_NR, proc_nr);
	GetAssignedResources(MEMORY, mem);
	logger->Notice("MayApp::onConfigure(): "
		"EXC [%s], AWM[%02d] => R<PROC_quota>=%3d, R<PROC_nr>=%2d, R<MEM>=%3d",
		exc_name.c_str(), awm_id, proc_quota, proc_nr, mem); // proc nr => num of threads 

	if(!cfg.enableSkip)
	{
		logger->Warn("Skipped State is not Enabled");
		return RTLIB_OK;
	}

	unsigned int skip_procs = (unsigned int) cfg.numOfThreads_Processors - h_procs((unsigned int) proc_nr); 
	unsigned int skip_sampl = (unsigned int) cfg.numOfThreads_Samplers   - h_procs((unsigned int) proc_nr); 
	unsigned int p_skipped = 0;
	unsigned int s_skipped = 0;

	// physical_numOfThreads
	physical_numOfThreads = numOfThreads - skip_procs - skip_sampl;
	for (auto & p : processors)
	{
		p->resume(); // refresh old skip state
		
		if(p_skipped < skip_procs)
		{
			p->skip();
			p_skipped++;
			logger->Notice("Skip proc %d/%d on %d totals", p_skipped, skip_procs, cfg.numOfThreads_Processors);
		}
	}

	for (auto & s : samplers)
	{
		s->resume();	// refresh old skip state

		if(s_skipped < skip_sampl)
		{
			s->skip();
			s_skipped++;
			logger->Notice("Skip sampl %d/%d on %d totals", s_skipped, skip_sampl, cfg.numOfThreads_Samplers);
		}
	}

	return RTLIB_OK;
}

RTLIB_ExitCode_t OpenCVCameraCalibration::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

		// Return after N Cycls
		if (Cycles() >= (unsigned int) cfg.numOfCycles)
			return RTLIB_EXC_WORKLOAD_NONE; // questa fa saltare al on-release()

		logger->Warn("OpenCVCameraCalibration::onRun()      : Cycle [%d] EXC [%s]  @ AWM [%02d]",
			Cycles(), exc_name.c_str(), wmp.awm_id);

	// ************************************************** //
	// * wake up all threads:
	//		- Cycle 0] signal all threads in Runqueue
	//		- Cycle *] signal all threads in Waitqueue 
	// * onRun strategy:
	//	  - Time] after a quantum of seconds,  threads are stopped
	//    - Image] after got result of # images, threads are stopped
	// * put threads in Wait state.
	// ************************************************** //
	unsigned int start_ = calibrationData.get_ReprojectionErrors_Size();

	if (Cycles() == 0)
	{	
		while(RunQueue.size() != numOfThreads) {}; // wait fino a quando tutti i thread hanno settato
		logger->Notice("Main - sveglia i thread creati");
		cv_master.notify_all();
	}
	else 
	{
		while(WaitQueue.size() != 0) { cv_master.notify_all(); }	// 
		logger->Notice("Main - sveglia i thread stoppati");
	}

	// OnRun strategy
	if(cfg.synchType == CalibrationConfig::TIME)
	{
		std::this_thread::sleep_for (std::chrono::seconds(cfg.timeCycle));
		logger->Notice("Main - scade il quanto di %d secondi", cfg.timeCycle);
	}
	else if (cfg.synchType == CalibrationConfig::NUM_IMAGE)
	{
		while(calibrationData.get_ReprojectionErrors_Size() - start_ < (unsigned int) cfg.imagesCycles) {}
		logger->Notice("Main - analizzate %d immagini", cfg.imagesCycles);
	}

	for (auto &p : processors)
	{
		p->pause();
	}

	for (auto &s : samplers)
	{
		s->pause();
	}

	while(WaitQueue.size() != physical_numOfThreads) {};
	logger->Notice("Main - ha messo in pausa i threads", time);

	return RTLIB_OK;
}

RTLIB_ExitCode_t OpenCVCameraCalibration::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Warn("OpenCVCameraCalibration::onMonitor()  : EXC [%s]  @ AWM [%02d] "
			"=> cycles [%d], CPS = %.2f",
		exc_name.c_str(), wmp.awm_id, Cycles(), GetCPS());

	return RTLIB_OK;
}

RTLIB_ExitCode_t OpenCVCameraCalibration::onSuspend() {

	logger->Warn("OpenCVCameraCalibration::onSuspend()  : suspension...");
	return RTLIB_OK;
}

// mettere qua la join
RTLIB_ExitCode_t OpenCVCameraCalibration::onRelease() {

	logger->Warn("OpenCVCameraCalibration::onRelease()  : exit");

	// ****************************** //
	// Join the threads								//
	// ****************************** //

	if(RunQueue.size() > 0 )
	{
		 // resume all skipped thread
		 for (auto & p : processors) { p->resume(); };
		 for (auto & s : samplers) 	 { s->resume(); };
		 
		 // riattiva i threads 
		 while(WaitQueue.size() != 0) { cv_master.notify_all(); }
	
		logger->Notice("Main chiude i threads\n");

		// li chiudo settando i quit
		for (auto &p : processors)
		{
			p->stop();
		}

		for (auto &s : samplers)
		{
			s->stop();
		}

		// wait che i thread siano chiusi
		while(ExitQueue.size() != numOfThreads) {};     // aspetto che tutti siano tornati

		logger->Notice("Main fa la join dei threads");
		
		for (auto &p : processors)
		{
			p->exit();
		}

		for (auto &s : samplers)
		{
			s->exit();
		}

		// print ending result
		Mat cameraMatrix = calibrationData.get_CameraMatrix();
		vector<float> reprjErr = calibrationData.get_ReprojectionErrors();

		print_result("CAMERA MATRIX_RISULTANTE", cameraMatrix);
		print_result_vector("REPROJ_ERRORS", reprjErr);
		
	}

	return RTLIB_OK;
}
