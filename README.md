# BBque Camera Calibration 
BBque Camera Calibration is an application developed for the BBqueRTRM.

### Requirements
This application needs `opencv`.
Check here on how to install Opencv:
  https://docs.opencv.org/4.4.0/d7/d9f/tutorial_linux_install.html

### How to Build
Go to bbque path and run the following code
```
$ make menuconfig
```
Then in the menu select `Application  --->` and check `OpenCVCameraCalibration`.
[Here you can see a menuconfig screenshot](https://gitlab.com/EnricoRuggiano/opencvcameracalibration/-/blob/master/doc/res/menuconfig.png)

So build bbque with the following command
```
$ make
```

### How to Run
Open the bbque console and run this command:

```
[BOSPShell] opencvcameracalibration -S <path_to_config_file>
```
Where `<path_to_config_file>` is the path of a `.xml` config file. 

### How to Configure
In `config/` directory  you can find some template for the input config file needed to run the application.
* `config/cameraConfig_IMG.xml` runs the calibration app using the `Number of Images` strategy.
* `config/cameraConfig_TM.xml` runs the calibration app using the `Time Based` strategy.

### Configuration Options
The configuration file is a `*.xml` file and has this structure:

```
    <BoardSize_Width>  </BoardSize_Width>
    <BoardSize_Height>  </BoardSize_Height>
    <ImageSize_Width></ImageSize_Width>
    <ImageSize_Height></ImageSize_Height>
    <Calibrate_Pattern> </Calibrate_Pattern>
    <Square_Size> </Square_Size>
    <Calibrate_FixAspectRatio> </Calibrate_FixAspectRatio>
    <Calibrate_AssumeZeroTangentialDistortion>  </Calibrate_AssumeZeroTangentialDistortion>
    <Calibrate_FixPrincipalPointAtTheCenter> </Calibrate_FixPrincipalPointAtTheCenter>
    <Calibrate_UseFisheyeModel> </Calibrate_UseFisheyeModel>
    <Input_FlipAroundHorizontalAxis>  </Input_FlipAroundHorizontalAxis>
    <Show_UndistortedImage>  </Show_UndistortedImage>
    <Input>  </Input>
    <Fix_K1> </Fix_K1>
    <Fix_K2> </Fix_K2>
    <Fix_K3> </Fix_K3>
    <Fix_K4> </Fix_K4>
    <Fix_K5> </Fix_K5>
    <imagesCycles> </imagesCycles>
    <timeCycle> </timeCycle>
    <numOfCycles> </numOfCycles>
    <numOfThreads_Processors> </numOfThreads_Processors>
    <numOfThreads_Samplers> </numOfThreads_Samplers>
    <synchType> </synchType>
    <enableSkip> </enableSkip>
```
The options related to BBQUE are:
* `<synchType>`: which specify the On Run synchronization strategy. Set it to `TIME` for a time based strategy or `NUM_IMAGE` for a number of image processed strategy  
* `<imagesCycles>`: which specify the number of images processed each OnRun cycle for a `NUM_IMAGE` strategy.
* `<timeCycles>`: which specify time quantum in milliseconds for a `TIME` based strategy.
* `<numOfThreads_Processors>`: which specify the number of threads Processors to use.
* `<numOfThreads_Samplers>`: which specify the number of threads Samplers to use.
* `<enableSkip>`: you can set this to 0 if you don't want that threads go to skip state 

The options related to input dataset are:
* `<Input>` : which specify the absolute path for the image list file where are present all the paths for the images to process. As input test dataset we used `config/image_list.xml`.  
* `<Calibrate_Pattern>`: specify which pattern should the samplers use to extract the image points (e.g `Chessboard`, `Cycle grids`, `Asymmetric circles grid`)
* `<ImageSize_Width>`, `<ImageSize_Height>`: specify the resolution of the input images. The input images must have all the same resolution.
* `<BoardSize_Width>`, `<BoardSize_Height>`: specify the number of elements that compose the pattern to use in the sampling.
* `<Square_Size>`: the physical size of the element that compose the pattern mask. 

Finally the other options are algorithm related settings.

## Documentation
For further details please check the documentation:
https://gitlab.com/EnricoRuggiano/opencvcameracalibration/-/blob/master/doc/report.pdf

## References
* Opencv Tutorial: https://docs.opencv.org/master/d4/d94/tutorial_camera_calibration.html
* Bbque: https://bosp.deib.polimi.it/doku.php?id=start
