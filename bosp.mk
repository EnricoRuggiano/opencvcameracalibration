
ifdef CONFIG_CONTRIB_OPENCVCAMERACALIBRATION

# Targets provided by this project
.PHONY: opencvcameracalibration clean_opencvcameracalibration

# Add this to the "contrib_testing" target
testing: opencvcameracalibration
clean_testing: clean_opencvcameracalibration

MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION=contrib/user/OpenCVCameraCalibration

opencvcameracalibration: external
	@echo
	@echo "==== Building OpenCVCameraCalibration ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_opencvcameracalibration:
	@echo
	@echo "==== Clean-up OpenCVCameraCalibration Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/opencvcameracalibration ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/OpenCVCameraCalibration*; \
		rm -f $(BUILD_DIR)/usr/bin/opencvcameracalibration*
	@rm -rf $(MODULE_CONTRIB_USER_OPENCVCAMERACALIBRATION)/build
	@echo

else # CONFIG_CONTRIB_OPENCVCAMERACALIBRATION

opencvcameracalibration:
	$(warning contib module OpenCVCameraCalibration disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_OPENCVCAMERACALIBRATION

