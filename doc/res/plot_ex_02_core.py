import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

n_samplers = [1 \
,2 \
,3 \
,4 \
,2 \
,3 \
,3 \
,2 \
,3 \
,4 \
,4 \
,5 \
,5 \
,6 \
,8 \
,12 \
,16 ]

n_processors = [1 \
,1 \
,1 \
,1 \
,2 \
,2 \
,3 \
,3 \
,4 \
,2 \
,4 \
,2 \
,4 \
,6 \
,3 \
,4 \
,6 ]

bbque_process_ms = [ 26736 \
,28496 \
,32766 \
,38936 \
,23823 \
,23794 \
,21502 \
,21671 \
,22844 \
,27287 \
,22571 \
,31788 \
,23736 \
,23263 \
,33113 \
,41538 \
,36025 \
]

z_min = min(bbque_process_ms)
z_max = max(bbque_process_ms)
z_baseline = np.array(26736)
col = np.where(bbque_process_ms<= z_baseline,'b','r')

ax.scatter(n_samplers, n_processors, bbque_process_ms, c=col, vmin=z_min, vmax=z_max, edgecolor='none',marker='o')
ax.set_xlabel('Num of Samplers')
ax.set_ylabel('Num of Processors')
ax.set_zlabel('Execution time [ms]')
ax.set_title('Dual Core configuration')

fig.savefig("execution_time_02_core.png")
plt.show()