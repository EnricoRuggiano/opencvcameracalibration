import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

n_samplers = [1 \
,2 \
,3 \
,4 \
,2 \
,3 \
,3 \
,2 \
,3 \
,4 \
,4 \
,5 \
,5 \
,6 \
,8 \
,12 \
,16 ]

n_processors = [1 \
,1 \
,1 \
,1 \
,2 \
,2 \
,3 \
,3 \
,4 \
,2 \
,4 \
,2 \
,4 \
,6 \
,3 \
,4 \
,6 ]

bbque_process_ms = [ 43025 \
,48854 \
,59584 \
,75507 \
,37800 \
,44216 \
,39410 \
,41359 \
,40161 \
,50199 \
,39822 \
,57219 \
,46094 \
,46888 \
,67230 \
,75701 \
,65170 \
]

z_min = min(bbque_process_ms)
z_max = max(bbque_process_ms)
z_baseline = np.array(43025)
col = np.where(bbque_process_ms<= z_baseline,'b','r')

ax.scatter(n_samplers, n_processors, bbque_process_ms, c=col, vmin=z_min, vmax=z_max, edgecolor='none',marker='o')
ax.set_xlabel('Num of Samplers')
ax.set_ylabel('Num of Processors')
ax.set_zlabel('Execution time [ms]')
ax.set_title('Single Core configuration')

fig.savefig("execution_time_01_core.png")
plt.show()